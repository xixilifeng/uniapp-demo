// 全局请求封装
const baseApi='http://192.168.50.212:8888'
export default (url, method, params) => {
const token = uni.getStorageSync('token');
	uni.showLoading({
		title: "加载中"
	});
	let routes = getCurrentPages(); // 获取当前打开过的页面路由数组
	let curRoute = routes[routes.length - 1].route // 获取当前页面路由
	if(!token && curRoute!=='pages/login/login'){
		// 除登录页以外------执行没有登录的逻辑
		return;
	}
	return new Promise((resolve, reject) => {
	
		uni.request({
			url: baseApi + url,
			method: method,
			header: {
				token: token
			},
			data: {
				...params
			},
			success(res) {
				// 可根据业务逻辑自定义拦截器
				resolve(res.data);
			},
			fail(err) {
				console.log(err)
				reject(err);
			},
			complete() {
				// 请求完成统一关闭loading
				uni.hideLoading();
			}
		});
	});
};
